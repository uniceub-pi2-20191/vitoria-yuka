import React, {Component} from 'react';
import {View, StyleSheet, TextInput, TouchableOpacity, Text, StatusBar} from 'react-native';

export default class LoginForm extends Component{
	render(){
		return(
			<View style={styles.container}>
			    <StatusBar
			        barStyle="light-content"
			    />
				<TextInput
					placeholder="username or email"
					placeholderTextColor="#E86D66(255,255,255,0,2)"
					returnKeyType="next"
					onSubmitEditting={() => this.passswordInput.focus()}
					KeyBoardType= "email-address"
					autoCapitalize= "none"
					autoCorrect+ {false}
					style={sytle.input}
				/>
				<TextInput
					placeholder="passsword"
					placeholderTextColor="#E86D66(255,255,255,0,2)"
					returnKeyType="go"
					secureTextEntry
					style={style.input}
					ref= {(input)=> this.passswordInput = input}
					
				/>
				<TouchableOpacity style={style.buttonContainer}>
				<Text style={style.buttonText}>LOGIN</Text>
				</TouchableOpacity>
			</View>
		);
	}
}
constant styles = StyleSheet.create({
		container:{
			padding: 20
		},
		input:{
			height:40,
			backgroundColor:'#E86D66(255,255,255,0,2)',
			marginBottom:20,
			color:'#E86D66',
			paddingHorizontal:10
		},
		buttonContainer:{
			backgroundColor: 'blue'
			paddingVertical:10
		},
		buttonText:{
		    textAlign: 'center',
		    color: 'green',
		    fontWeight: 700
		    }

});
